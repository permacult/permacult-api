import { dataSourceOptionsNlmSqlite } from 'ormconfig.nlm.sqlite';
import { dataSourceOptionsPermacultSqlite } from 'ormconfig.permacult.sqlite';

// see https://stackoverflow.com/questions/51994541/nestjs-typeorm-use-two-or-more-databases

const ormConfig = [
  dataSourceOptionsPermacultSqlite,
  dataSourceOptionsNlmSqlite,
];

export default ormConfig;
