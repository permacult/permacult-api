import os from 'os';
import { DataSourceOptions } from 'typeorm';
import 'dotenv/config';
import { DATABASE_CONNECTION } from 'src/shared/constants';
import { join } from 'path';

// const dataSourceOptions: DataSourceOptions = {
//   type: 'postgres',
//   // host: '/run/postgresql', // socket
//   host: process.env.DB_HOST || '/run/postgresql',
//   port: Number(process.env.DB_PORT) || 5432,
//   username: process.env.DB_USER || os.userInfo().username,
//   password: process.env.DB_PASS || undefined,
//   database: process.env.DB_NAME || 'ofil_nest',
//   schema: process.env.DB_SCHEMA || 'public',
//   entities: [__dirname + '/**/*.entity{.ts,.js}'],
//   // entities: [__dirname + '/**/*.entity{.ts,.js}'],
//   synchronize: true,
//   dropSchema: true,
//   logging: true,
//   // namingStrategy: new SnakeNamingStrategy(),
// };

export const dataSourceOptionsPermacultSqlite: DataSourceOptions = {
  // name: 'default',
  name: DATABASE_CONNECTION,
  type: 'better-sqlite3',
  database: process.env.DB_NAME || 'data/permacult.sqlite',
  entities: [
    join(__dirname, 'src/shared/entities/cult/**/*.entity.{ts,js}'),
    join(__dirname, 'src/shared/entities/permacult/**/*.entity.{ts,js}'),
  ],
  subscribers: ['dist/**/**.subscriber{.ts,.js}'],
  migrations: ['migrations/*.ts'],
  synchronize: true,
  // dropSchema: true,
  logging: true,
};

// export default dataSourceOptionsSqlite;
