CREATE COLLATION IF NOT EXISTS public."case_insensitive" (provider = icu, locale = 'und-u-ks-level2', deterministic = false);
CREATE COLLATION IF NOT EXISTS public."ignore_accents" (provider = icu, locale = 'und-u-ks-level1-kc-true', deterministic = false);
CREATE COLLATION IF NOT EXISTS public."ignore_accents_ci" (provider = icu, locale = 'und-u-ks-level2-kc-true', deterministic = false);

CREATE COLLATION IF NOT EXISTS public."NOCASE" FROM public."ignore_accents_ci";
