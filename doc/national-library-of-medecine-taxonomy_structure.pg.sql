--
-- PostgreSQL database dump
--

-- Dumped from database version 13.7 (Debian 13.7-1.pgdg100+1)
-- Dumped by pg_dump version 13.7 (Debian 13.7-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: genCode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."genCode" (
    id integer NOT NULL,
    abbreviation text,
    name text NOT NULL,
    cde text,
    starts text
);


--
-- Name: taxDivision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."taxDivision" (
    id integer NOT NULL,
    code text NOT NULL,
    name text,
    comment text
);


--
-- Name: taxName; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."taxName" (
    "taxNodeId" integer NOT NULL,
    name text NOT NULL,
    "nameUnique" text,
    class text,
    id integer NOT NULL
);


--
-- Name: taxNode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."taxNode" (
    id integer NOT NULL,
    "parentId" integer NOT NULL,
    rank text NOT NULL,
    "emblCode" text,
    "taxDivisionId" integer NOT NULL,
    "inheritedDivisionFlag" integer DEFAULT 0 NOT NULL,
    "genCodeId" integer NOT NULL,
    "inheritedGenCodeFlag" integer DEFAULT 0 NOT NULL,
    "mitochondrialGenCodeId" integer NOT NULL,
    "inheritedMitochondrialGenCodeFlag" integer DEFAULT 0 NOT NULL,
    "genBankHiddenFlag" integer NOT NULL,
    "hiddenSubtreeRootFlag" integer NOT NULL,
    comment text
);


--
-- Name: genCode genCode_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."genCode"
    ADD CONSTRAINT "genCode_pkey" PRIMARY KEY (id);


--
-- Name: taxDivision taxDivision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."taxDivision"
    ADD CONSTRAINT "taxDivision_pkey" PRIMARY KEY (id);


--
-- Name: taxName taxName_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."taxName"
    ADD CONSTRAINT "taxName_pkey" PRIMARY KEY (id);


--
-- Name: taxNode taxNode_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."taxNode"
    ADD CONSTRAINT "taxNode_pkey" PRIMARY KEY (id);


--
-- Name: fki_lnk_taxNode_taxName; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "fki_lnk_taxNode_taxName" ON public."taxName" USING btree ("taxNodeId");


--
-- Name: fki_lnk_taxNode_taxNode; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "fki_lnk_taxNode_taxNode" ON public."taxNode" USING btree ("parentId");


--
-- Name: taxName_taxNodeId_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "taxName_taxNodeId_idx" ON public."taxName" USING btree ("taxNodeId");


--
-- Name: taxNode_parentId_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "taxNode_parentId_idx" ON public."taxNode" USING btree ("parentId");


--
-- Name: taxNode lnk_taxDivision_taxNode; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."taxNode"
    ADD CONSTRAINT "lnk_taxDivision_taxNode" FOREIGN KEY ("taxDivisionId") REFERENCES public."taxDivision"(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: taxName lnk_taxNode_taxName; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."taxName"
    ADD CONSTRAINT "lnk_taxNode_taxName" FOREIGN KEY ("taxNodeId") REFERENCES public."taxNode"(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE NOT VALID;


--
-- Name: taxNode lnk_taxNode_taxNode; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."taxNode"
    ADD CONSTRAINT "lnk_taxNode_taxNode" FOREIGN KEY ("parentId") REFERENCES public."taxNode"(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE NOT VALID;


--
-- PostgreSQL database dump complete
--
