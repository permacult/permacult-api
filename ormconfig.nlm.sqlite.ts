import os from 'os';
import { DataSourceOptions } from 'typeorm';
import 'dotenv/config';
import { join } from 'path';
import { NLM_DATABASE_CONNECTION } from 'src/shared/constants';

// const dataSourceOptions: DataSourceOptions = {
//   type: 'postgres',
//   // host: '/run/postgresql', // socket
//   host: process.env.DB_HOST || '/run/postgresql',
//   port: Number(process.env.DB_PORT) || 5432,
//   username: process.env.DB_USER || os.userInfo().username,
//   password: process.env.DB_PASS || undefined,
//   database: process.env.DB_NAME || 'permacult_nlm',
//   schema: process.env.DB_SCHEMA || 'public',
//   entities: [__dirname + '/**/*.entity{.ts,.js}'],
//   // entities: [__dirname + '/**/*.entity{.ts,.js}'],
//   synchronize: true,
//   dropSchema: true,
//   logging: true,
//   // namingStrategy: new SnakeNamingStrategy(),
// };

export const dataSourceOptionsNlmSqlite: DataSourceOptions = {
  // name: 'nlm-taxonomy',
  name: NLM_DATABASE_CONNECTION,
  type: 'better-sqlite3',
  database: process.env.NLM_DB_NAME || 'data/nlmTaxonomy.sqlite',
  entities: [
    join(__dirname, 'src/shared/entities/nlm-taxonomy/**/*.entity.{ts,js}'),
  ],
  // entities: [__dirname + '/**/*.entity{.ts,.js}'],
  // subscribers: ['dist/**/**.subscriber{.ts,.js}'],
  // migrations: ['migrations/*.ts'],
  // synchronize: true,
  // dropSchema: true,
  // logging: true,
};
