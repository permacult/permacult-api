# PermaCult API

# Very much in development process... at your own risks.

## Presentation

A dedicated decision analysis application for permaculture.
Initially focused on plants and their 'compatibilities' the database will aim to be universal and should be flexible enough to refer anything that can play a role in gardening. Insects or mushrooms just to name a couple.

### Universal Taxonomic Classification

<img src="doc/img/Venus_flytrap_taxonomy-1024x579.webp" style="background-color: white; padding: 1em;">
Source https://carnivorousplantresource.com/carnivorous-plant-taxonomy/

<img src="doc/img/Taxonomic_Rank_Graph.svg" style="background-color: white; padding: 1em;">

Source: https://en.wikipedia.org/wiki/Taxonomic_rank

---

### Plants

- latin name
- common name
- layer location in a forest
- ‌a hardiness zone
- friends / foes
- A 'seed mix' has a list of plants (ex: functions for the soil and fauna)

---

L'outil se veut polyvalent et ouvert à l'ensemble du monde vivant, à commencer par les végétaux puis intégrer le royaume des insectes ou des champignons.

## Requirements

- nodejs v16.16+ recommended

## Quick start

- download National Library of Medicine sqlite database
  https://ftp.openpermacult.org/nlm-taxonomy/

  ```bash
  $ bzcat nlmTaxonomy.dump.sqlite.sql.bz2 | sqlite3 data/nlmTaxonomy.sqlite
  ```

```bash
$ git clone git@gitlab.com:permacult/permacult-api.git
$ cd permacult-api
$ npm i
$ npm run start:dev
```

More details on Nestjs distribution [README](README.dist.md)

## DB Model

### All happens around `cult_content` and `cult_node`

Note that the grid is `nodeId <> toNodeId` when usually we refer to ancestor not descendant.

To fit the desire that contents are attached to `nodeId` no mater who they target. So "Tomatoes helps celery", `nodeId` is the one of Tomatoes.

In // there is also a `parentId` + `mpath` managed by TypeORM@Tree('materialized-path') _(aka Path Enumeration)_ to handle a simple tree of what labels appear in navigation.

<img src="doc/img/db-cult-cult_content-2022-08-20_22-31.png" />

### Keeping `cult_node` as light as possible

Centered on `cult_node`, all the common calls are in direct reach

- Featured image
- Any linked content ascendant or descendant
- Related `References` and `Links` using efficient indices.

- Chose to identify Node.id as `INT` and Content.id using `UUID`
  - Lighter in DB and RAM fingerprint.
  - The app/site is content centered, having a strong key to hold the data seemed to make sense... Might not, but it can still help avoid confusions during dev process.

<picture>
  <img src="doc/img/db-cult-cult_node-2022-08-20_23-16.png" />
</picture>

### Here the full vue to date 2022-08

<img src="doc/img/db-cult.svg" style="background: white;"/>

```bash
# just to have it handy. This utility saved my life...
# Explore any database like a charm and spot mistakes like gbifID :)

java -jar ~/bin/schemaspy-6.1.0.jar -t pgsql11 -s public -vizjs -dp ~/bin/postgresql-42.2.18.jar -db permacult_cult -host localhost -u dbUser -p dbPass -o ./tmp/schema-output

```

---

### Authors

- Peter Geirnaert @freqrush

  Project initiator.<br>
  Patinated permaculture gardener.

- Antony GIBBS @cantoute

  Software architect.<br>
  A complexe data driven application... Making it simple to use, my favorite playground :)

  I love my food. I love my planet.<br>
  Any thing else we should changed into compost...<br>
  Bother my worms and you'll soon be on their menu.<br>
  You are warned!
