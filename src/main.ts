import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { seeder } from './database/seeder';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      // origin: 'https://web.example.org',
      origin: '*',

      // required for cookie auth
      // credentials: true,
    },
  });

  const configService = app.get<ConfigService>(ConfigService);

  // app.setGlobalPrefix('api');

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
      stopAtFirstError: false,
    }),
  );

  const swaggerOptions = new DocumentBuilder()
    .setTitle('PermaCult API')
    .setContact(
      'PermaCult',
      'https://gitlab.com/permacult/permacult-api',
      'contact@openpermacult.org',
    )
    .setDescription('General Taxonomy')
    .setVersion(process.env.npm_package_version)
    // .addTag('PermaCult API')
    .addTag(
      'NLM',
      'National Library of Medicine Taxonomy <a href="https://www.ncbi.nlm.nih.gov/taxonomy" target="_blank">source</a>',
    )
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token',
    )
    .build();

  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('swagger', app, swaggerDocument);

  // await seeder(app); // .then((result) => console.log(result));

  const PORT = configService.get('PORT') || 3000;

  await app.listen(PORT);

  console.log(
    `Swagger loaded: try out the api here http://localhost:${PORT}/swagger/`,
  );
}
bootstrap();
