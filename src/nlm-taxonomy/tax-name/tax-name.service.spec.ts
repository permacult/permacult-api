import { Test, TestingModule } from '@nestjs/testing';
import { TaxNameService } from './tax-name.service';

describe('TaxNameService', () => {
  let service: TaxNameService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaxNameService],
    }).compile();

    service = module.get<TaxNameService>(TaxNameService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
