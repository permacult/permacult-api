import { Module } from '@nestjs/common';
import { TaxNameService } from './tax-name.service';
import { TaxNameController } from './tax-name.controller';

@Module({
  controllers: [TaxNameController],
  providers: [TaxNameService]
})
export class TaxNameModule {}
