import { Test, TestingModule } from '@nestjs/testing';
import { TaxNameController } from './tax-name.controller';
import { TaxNameService } from './tax-name.service';

describe('TaxNameController', () => {
  let controller: TaxNameController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaxNameController],
      providers: [TaxNameService],
    }).compile();

    controller = module.get<TaxNameController>(TaxNameController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
