import { PartialType } from '@nestjs/swagger';
import { CreateTaxNameDto } from './create-tax-name.dto';

export class UpdateTaxNameDto extends PartialType(CreateTaxNameDto) {}
