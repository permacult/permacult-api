import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TaxNameService } from './tax-name.service';
import { CreateTaxNameDto } from './dto/create-tax-name.dto';
import { UpdateTaxNameDto } from './dto/update-tax-name.dto';
import { NLM_API_BASE } from 'src/shared/constants';

@Controller([NLM_API_BASE, 'tax-name'].join('/'))
export class TaxNameController {
  constructor(private readonly taxNameService: TaxNameService) {}

  // @Post()
  // create(@Body() createTaxNameDto: CreateTaxNameDto) {
  //   return this.taxNameService.create(createTaxNameDto);
  // }

  // @Get()
  // findAll() {
  //   return this.taxNameService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.taxNameService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateTaxNameDto: UpdateTaxNameDto) {
  //   return this.taxNameService.update(+id, updateTaxNameDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.taxNameService.remove(+id);
  // }
}
