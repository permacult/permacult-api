import { Injectable } from '@nestjs/common';
import { CreateTaxNameDto } from './dto/create-tax-name.dto';
import { UpdateTaxNameDto } from './dto/update-tax-name.dto';

@Injectable()
export class TaxNameService {
  create(createTaxNameDto: CreateTaxNameDto) {
    return 'This action adds a new taxName';
  }

  findAll() {
    return `This action returns all taxName`;
  }

  findOne(id: number) {
    return `This action returns a #${id} taxName`;
  }

  update(id: number, updateTaxNameDto: UpdateTaxNameDto) {
    return `This action updates a #${id} taxName`;
  }

  remove(id: number) {
    return `This action removes a #${id} taxName`;
  }
}
