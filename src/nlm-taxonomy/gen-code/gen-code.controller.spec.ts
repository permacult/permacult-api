import { Test, TestingModule } from '@nestjs/testing';
import { GenCodeController } from './gen-code.controller';
import { GenCodeService } from './gen-code.service';

describe('GenCodeController', () => {
  let controller: GenCodeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GenCodeController],
      providers: [GenCodeService],
    }).compile();

    controller = module.get<GenCodeController>(GenCodeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
