import { Module } from '@nestjs/common';
import { GenCodeService } from './gen-code.service';
import { GenCodeController } from './gen-code.controller';

@Module({
  controllers: [GenCodeController],
  providers: [GenCodeService]
})
export class GenCodeModule {}
