import { Injectable } from '@nestjs/common';
import { CreateGenCodeDto } from './dto/create-gen-code.dto';
import { UpdateGenCodeDto } from './dto/update-gen-code.dto';

@Injectable()
export class GenCodeService {
  create(createGenCodeDto: CreateGenCodeDto) {
    return 'This action adds a new genCode';
  }

  findAll() {
    return `This action returns all genCode`;
  }

  findOne(id: number) {
    return `This action returns a #${id} genCode`;
  }

  update(id: number, updateGenCodeDto: UpdateGenCodeDto) {
    return `This action updates a #${id} genCode`;
  }

  remove(id: number) {
    return `This action removes a #${id} genCode`;
  }
}
