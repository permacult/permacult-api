import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { GenCodeService } from './gen-code.service';
import { CreateGenCodeDto } from './dto/create-gen-code.dto';
import { UpdateGenCodeDto } from './dto/update-gen-code.dto';
import { NLM_API_BASE } from 'src/shared/constants';

@Controller([NLM_API_BASE, 'gen-code'].join('/'))
export class GenCodeController {
  constructor(private readonly genCodeService: GenCodeService) {}

  // @Post()
  // create(@Body() createGenCodeDto: CreateGenCodeDto) {
  //   return this.genCodeService.create(createGenCodeDto);
  // }

  // @Get()
  // findAll() {
  //   return this.genCodeService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.genCodeService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateGenCodeDto: UpdateGenCodeDto) {
  //   return this.genCodeService.update(+id, updateGenCodeDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.genCodeService.remove(+id);
  // }
}
