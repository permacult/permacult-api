import { PartialType } from '@nestjs/swagger';
import { CreateGenCodeDto } from './create-gen-code.dto';

export class UpdateGenCodeDto extends PartialType(CreateGenCodeDto) {}
