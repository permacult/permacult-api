import { Test, TestingModule } from '@nestjs/testing';
import { GenCodeService } from './gen-code.service';

describe('GenCodeService', () => {
  let service: GenCodeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GenCodeService],
    }).compile();

    service = module.get<GenCodeService>(GenCodeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
