import { Module } from '@nestjs/common';
import { TaxNodeModule } from './tax-node/tax-node.module';
import { TaxNameModule } from './tax-name/tax-name.module';
import { TaxDivisionModule } from './tax-division/tax-division.module';
import { GenCodeModule } from './gen-code/gen-code.module';
import { DatabaseModule } from 'src/database/database.module';
import { nlmTaxonomyProviders } from 'src/database/database.providers';

@Module({
  imports: [
    DatabaseModule,
    TaxNodeModule,
    // TaxNameModule,
    // TaxDivisionModule,
    // GenCodeModule,
  ],
  providers: [],
  // exports: [...nlmTaxonomyProviders],
})
export class NlmTaxonomyModule {}

// @Module({
//   imports: [DatabaseModule],
//   controllers: [TaxonomiesController],
//   providers: [...permacultProviders, TaxonomiesService],
// })
