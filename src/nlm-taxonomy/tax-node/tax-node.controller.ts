import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  BadRequestException,
  NotFoundException,
  Query,
  HttpException,
} from '@nestjs/common';
import { TaxNodeJoin, TaxNodeService } from './tax-node.service';
import { CreateTaxNodeDto } from './dto/create-tax-node.dto';
import { UpdateTaxNodeDto } from './dto/update-tax-node.dto';
import { NLM_API_BASE } from 'src/shared/constants';
import {
  ApiBadRequestResponse,
  ApiDefaultResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiParam,
  ApiProperty,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  TaxNode,
  TaxNodeRank,
} from 'src/shared/entities/nlm-taxonomy/TaxNode.entity';
import { TaxDivisionName } from 'src/shared/entities/nlm-taxonomy/TaxDivision.entity';
import {
  defaultPaginatedOptions,
  PaginatedOptionsDto,
} from 'src/shared/dto/paginated-options.dto';
import { PaginatedDto } from 'src/shared/dto/paginated.dto';
import {
  IsArray,
  IsEnum,
  IsInt,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { Type } from 'class-transformer';

export class FindByNameQueryDto extends PaginatedOptionsDto {
  @ApiProperty({
    required: true,
  })
  @IsString()
  q: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsEnum(TaxDivisionName)
  division: TaxDivisionName;

  @IsOptional()
  rank?: TaxNodeRank | TaxNodeRank[];

  @IsOptional()
  join?: TaxNodeJoin | TaxNodeJoin[];

  @IsOptional()
  @IsInt()
  @Min(0)
  ancestorsLimitDepth?: number;

  @ApiProperty({
    required: false,
    isArray: true,
  })
  @IsEnum(TaxNodeRank, { each: true })
  @IsOptional()
  ancestorsLimitRank: TaxNodeRank | TaxNodeRank[];
}
@Controller([NLM_API_BASE, 'tax-node'].join('/'))
@ApiTags('NLM')
export class TaxNodeController {
  constructor(private readonly taxNodeService: TaxNodeService) {}

  // @Post()
  // create(@Body() createTaxNodeDto: CreateTaxNodeDto) {
  //   return this.taxNodeService.create(createTaxNodeDto);
  // }

  // @Get()
  // findAll() {
  //   return this.taxNodeService.findAll();
  // }

  @Get('roots')
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiResponse({ status: 200, type: [TaxNode] })
  async findRoots() {
    return this.taxNodeService.findRoots().catch((error) => {
      console.error('taxNode.findRoots', error);

      throw new BadRequestException(`Bad request`);
    });
  }

  @Get('findByIds')
  @ApiBadRequestResponse({ description: 'Bad request' })
  // @ApiNotFoundResponse({ description: 'Not found' })
  @ApiResponse({ status: 200, type: [TaxNode] })
  @ApiQuery({
    name: 'id',
    isArray: true,
    required: true,
    type: String,
    example: '1234,2345',
  })
  @ApiQuery({
    name: 'join',
    isArray: true,
    required: false,
    enum: TaxNodeJoin,
    enumName: 'TaxNodeJoin',
  })
  @ApiQuery({
    name: 'ancestorsLimitDepth',
    required: false,
    type: 'number',
  })
  @ApiQuery({
    name: 'ancestorsLimitRank',
    isArray: true,
    required: false,
    enum: TaxNodeRank,
    enumName: 'TaxNodeRank',
  })
  async findByIds(
    @Query('id') id: string[] = [],
    @Query('join') join: TaxNodeJoin | TaxNodeJoin[],
    @Query('ancestorsLimitDepth') ancestorsLimitDepth: number,
    @Query('ancestorsLimitRank')
    ancestorsLimitRank: TaxNodeRank | TaxNodeRank[],
  ) {
    const result = await this.taxNodeService
      .findByIds(id, {
        join,
        ancestorsLimitDepth,
        ancestorsLimitRank,
      })
      .catch((error) => {
        console.error('taxNode.findByIds', id, error);

        throw new BadRequestException(`Bad request id:${id}`);
      });

    return result;
  }

  @Get('findByName')
  @ApiBadRequestResponse({ description: 'Bad request' })
  // @ApiNotFoundResponse({ description: 'Not found' })
  @ApiResponse({ status: 200, type: PaginatedDto<TaxNode> })
  @ApiQuery({
    name: 'q',
    required: true,
    type: String,
    description: '%Tomato%',
    // examples: { tomato: { value: '%Tomato%' } },
  })
  @ApiQuery({
    name: 'rank',
    required: false,
    enum: TaxNodeRank,
    enumName: 'TaxNodeRank',
    isArray: true,
  })
  @ApiQuery({
    name: 'division',
    required: false,
    enum: TaxDivisionName,
    enumName: 'TaxDivisionName',
    isArray: true,
  })
  @ApiQuery({
    name: 'join',
    isArray: true,
    required: false,
    enum: TaxNodeJoin,
    // enumName: 'TaxNodeJoin',
  })
  @ApiQuery({
    name: 'ancestorsLimitDepth',
    required: false,
    type: 'number',
  })
  @ApiQuery({
    name: 'ancestorsLimitRank',
    isArray: true,
    required: false,
    enum: TaxNodeRank,
    enumName: 'TaxNodeRank',
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: 'number',
    // description: 'Maximum 500',
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: 'number',
    // description: 'Minimum 0',
  })
  async findByName(
    // @Query('q') q: string,
    // @Query('division') divisionName: TaxDivisionName | TaxDivisionName[],
    // @Query('rank') rank: TaxNodeRank | TaxNodeRank[],
    // @Query('join') join: TaxNodeJoin | TaxNodeJoin[],
    // @Query('ancestorsLimitDepth') ancestorsLimitDepth: number,
    // @Query('ancestorsLimitRank')
    // ancestorsLimitRank: TaxNodeRank | TaxNodeRank[],
    // @Query('limit')
    // limit: PaginatedOptionsDto['limit'],
    // @Query('offset')
    // offset: PaginatedOptionsDto['offset'],

    @Query() query: FindByNameQueryDto,
  ): Promise<PaginatedDto<TaxNode>> {
    const result = await this.taxNodeService
      .findByName(
        query.q,
        {
          rank: query.rank,
          join: query.join,
          ancestorsLimitDepth: query.ancestorsLimitDepth,
          ancestorsLimitRank: query.ancestorsLimitRank,
          divisionName: query.division,
        },
        {
          limit: query.limit,
          offset: query.offset,
        },
      )
      .catch((error) => {
        console.error('taxNode.findByName', query.q, error);

        throw new BadRequestException(`Bad request name:'${query.q}'`);
      });

    return result;
  }

  @Get(':id')
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiQuery({
    name: 'join',
    isArray: true,
    required: false,
    enum: TaxNodeJoin,
    enumName: 'TaxNodeJoin',
  })
  @ApiQuery({
    name: 'ancestorsLimitDepth',
    required: false,
    type: 'number',
  })
  @ApiQuery({
    name: 'ancestorsLimitRank',
    isArray: true,
    required: false,
    enum: TaxNodeRank,
    enumName: 'TaxNodeRank',
  })
  async findOne(
    @Param('id') id: string,
    @Query('join') join: TaxNodeJoin | TaxNodeJoin[],
    @Query('ancestorsLimitDepth') ancestorsLimitDepth,
    @Query('ancestorsLimitRank')
    ancestorsLimitRank: TaxNodeRank | TaxNodeRank[],
  ) {
    const joins: TaxNodeJoin[] = Array.isArray(join) ? join : [join];

    const result = await this.taxNodeService
      .findOne(+id, { join, ancestorsLimitDepth, ancestorsLimitRank })
      .catch((error) => {
        console.error('taxNode.findOne', id, error);

        throw new BadRequestException(`Bad request id:${id}`);
      });

    if (result === null) {
      throw new NotFoundException(`Not found id:${id}`);
    }

    return result;
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateTaxNodeDto: UpdateTaxNodeDto) {
  //   return this.taxNodeService.update(+id, updateTaxNodeDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.taxNodeService.remove(+id);
  // }
}
