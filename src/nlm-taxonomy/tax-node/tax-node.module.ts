import { Module } from '@nestjs/common';
import { TaxNodeService } from './tax-node.service';
import { TaxNodeController } from './tax-node.controller';
import { DatabaseModule } from 'src/database/database.module';
import { nlmTaxonomyProviders } from 'src/database/database.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [TaxNodeController],
  providers: [TaxNodeService],
})
export class TaxNodeModule {}
