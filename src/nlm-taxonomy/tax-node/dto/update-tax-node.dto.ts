import { PartialType } from '@nestjs/swagger';
import { CreateTaxNodeDto } from './create-tax-node.dto';

export class UpdateTaxNodeDto extends PartialType(CreateTaxNodeDto) {}
