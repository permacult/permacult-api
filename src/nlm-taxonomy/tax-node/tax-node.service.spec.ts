import { Test, TestingModule } from '@nestjs/testing';
import { TaxNodeService } from './tax-node.service';

describe('TaxNodeService', () => {
  let service: TaxNodeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaxNodeService],
    }).compile();

    service = module.get<TaxNodeService>(TaxNodeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
