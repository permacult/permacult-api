import { Test, TestingModule } from '@nestjs/testing';
import { TaxNodeController } from './tax-node.controller';
import { TaxNodeService } from './tax-node.service';

describe('TaxNodeController', () => {
  let controller: TaxNodeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaxNodeController],
      providers: [TaxNodeService],
    }).compile();

    controller = module.get<TaxNodeController>(TaxNodeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
