import { Inject, Injectable } from '@nestjs/common';
import {
  NLM_DATABASE_CONNECTION,
  rootTaxNodeId,
  TAX_CULT_NODE_REPOSITORY,
} from 'src/shared/constants';
import { PaginatedMetaDto } from 'src/shared/dto/paginated-meta.dto';
import {
  defaultPaginatedOptions,
  PaginatedOptionsDto,
} from 'src/shared/dto/paginated-options.dto';
import { PaginatedDto } from 'src/shared/dto/paginated.dto';
import { TaxDivisionName } from 'src/shared/entities/nlm-taxonomy/TaxDivision.entity';
import {
  TaxName,
  TaxNameClass,
} from 'src/shared/entities/nlm-taxonomy/TaxName.entity';
import {
  TaxNode,
  TaxNodeRank,
} from 'src/shared/entities/nlm-taxonomy/TaxNode.entity';
import {
  DataSource,
  In,
  IsNull,
  Like,
  Not,
  QueryBuilder,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import { CreateTaxNodeDto } from './dto/create-tax-node.dto';
import { UpdateTaxNodeDto } from './dto/update-tax-node.dto';
import { stripAccents } from './utils/strip-accents';

export enum TaxNodeJoin {
  division = 'division',
  genCode = 'genCode',
  mitochondrialGenCode = 'mitochondrialGenCode',
  names = 'names',
  commonNames = 'commonNames',
  scientificNames = 'scientificNames',
  parent = 'parent',
  parentNames = 'parent.names',
  parentAncestors = 'parent.ancestors',
  parentAncestorsNames = 'parent.ancestors.names',
  parentFlatAncestors = 'parent.flatAncestors',
  children = 'children',
  childrenGenCode = 'children.genCode',
  childrenDivision = 'children.division',
  childrenNames = 'children.names',
  childrenCommonNames = 'children.commonNames',
  childrenScientificNames = 'children.scientificNames',
  childrenMitochondrialGenCode = 'children.mitochondrialGenCode',
}

export class TaxNodeFindOptions {
  join?: TaxNodeJoin | TaxNodeJoin[];
  rank?: TaxNodeRank | TaxNodeRank[];
  ancestorsLimitDepth?: number;
  ancestorsLimitRank?: TaxNodeRank | TaxNodeRank[];
  divisionName?: TaxDivisionName | TaxDivisionName[];
}

export const taxNodeFindOptionsDefault: TaxNodeFindOptions = {
  join: [],
  rank: [],
  ancestorsLimitDepth: undefined,
  ancestorsLimitRank: undefined,
  divisionName: undefined,
};

export type StringOrNumber = string | number;

@Injectable()
export class TaxNodeService {
  constructor(
    @Inject(NLM_DATABASE_CONNECTION)
    private readonly connection: DataSource,
    @Inject(TAX_CULT_NODE_REPOSITORY)
    private readonly taxNodeRepository: Repository<TaxNode>,
  ) {}

  async findRoots(): Promise<TaxNode[]> {
    const qb = this.taxNodeRepository
      .createQueryBuilder('node')
      .select()

      .leftJoinAndSelect('node.names', 'names')
      .leftJoinAndSelect('node.division', 'division')
      .leftJoinAndSelect('node.genCode', 'genCode')
      .leftJoinAndSelect('node.mitochondrialGenCode', 'mitochondrialGenCode')

      .leftJoinAndSelect(
        'node.children',
        'children',
        'children.id != node.id', // exclude parent from children
      )
      .leftJoinAndSelect('children.names', 'childNames')
      .leftJoinAndSelect('children.division', 'childDivision')
      .leftJoinAndSelect('children.genCode', 'childGenCode')
      .leftJoinAndSelect(
        'children.mitochondrialGenCode',
        'childrenMitochondrialGenCode',
      )

      .where('node.id = node.parentId') // id: 1 parentId: 1 is the root
      .orWhere({ parentId: IsNull() }); // as a fallback

    return qb.getMany();
  }

  async findOne(
    id: number,
    {
      join,
      ancestorsLimitDepth,
      ancestorsLimitRank,
    }: TaxNodeFindOptions = taxNodeFindOptionsDefault,
  ): Promise<TaxNode | null> {
    const result = await this.findByIds(id, {
      join,
      ancestorsLimitDepth,
      ancestorsLimitRank,
    });

    return result && result.length ? result[0] : null;
  }

  async findByName(
    q: string,
    taxNodeFindOptions: TaxNodeFindOptions = taxNodeFindOptionsDefault,
    paginatedOptionsDto: PaginatedOptionsDto = defaultPaginatedOptions,
  ): Promise<PaginatedDto<TaxNode>> {
    // const excludeIds: number[] = [];

    const qClean = stripAccents(q)
      .replace(/[\s%_]+/g, ' ') // any wildcard repeated replaced by ' '
      .replace(/^[\s%_]+|[\s%_]+$/, '');

    // if (taxNodeFindOptions.joins.includes(TaxNodeJoin.names)) {
    //   taxNodeFindOptions.joins.push(TaxNodeJoin.names);
    // }

    // const nameExactMatches = await this.taxNodeRepository
    //   .createQueryBuilder('node')
    //   .select('node.id', 'id')
    //   .distinct(true)
    //   .innerJoin(
    //     'node.names',
    //     'names',
    //     'names.name = :qClean', // Exact match
    //     {
    //       qClean,
    //     },
    //   )
    //   .limit(10)
    //   .getRawMany();

    // const nameExactMatchIds = nameExactMatches.map((e) => +e.id);

    // console.log('nameExactMatchIds', nameExactMatchIds, qClean);

    // const qStartsWith = qClean + '%';

    // const nameStartsWith = await this.taxNodeRepository
    //   .createQueryBuilder('node')
    //   .select('node.id', 'id')
    //   .distinct(true)
    //   .innerJoin(
    //     'node.names',
    //     'names',
    //     'names.name LIKE :qStartsWith', // Exact match
    //     {
    //       qStartsWith,
    //     },
    //   )
    //   .limit(10)
    //   .getRawMany();

    // const nameStartsWithIds = nameStartsWith.map((e) => +e.id);

    // console.log('nameStartsWithIds', nameStartsWithIds, qStartsWith);

    const qb = this.taxNodeRepository
      .createQueryBuilder('node')
      .select()
      .innerJoin('node.names', 'nameFilter', 'nameFilter.name LIKE :q', {
        q,
        // qClean: q.replace(/^[\s%_]+|[\s%_]+$/g, ''),
        // nameExactMatchIds,
        // nameExactMatchId: nameExactMatchIds[0],
        // nameStartsWithIds,
      });

    // exclude exact matches

    // if (excludeIds.length)
    //   qb.orderBy('node.id Not In(:...excludeIds) }', {
    //     excludeIds: nameExactMatchIds.concat(nameStartsWithIds),
    //   });

    // qb.leftJoin(
    //   'node.names',
    //   'nameExactMatch',
    //   'nameExactMatch.name = :qClean',
    //   { qClean },
    // );

    // // qb.select('', 'match');

    // qb.orderBy('(nameExactMatch.name IS NULL)', 'ASC');

    // if (nameExactMatchIds.length == 1)
    //   qb.orderBy('(node.id = :nameExactMatchId)', 'DESC');

    // if (nameExactMatchIds.length > 1)
    //   qb.orderBy('(node.id IN(:...nameExactMatchIds))', 'DESC');

    // if (nameStartsWithIds.length)
    //   qb.addOrderBy('node.id IN(:...nameStartsWithIds)', 'DESC');

    // .addSelect('nameFilter.name', 'nameFilterName')
    // .addSelect('nameFilter.name = :qClean', 'exactMatch');

    // qb.orderBy(`exactMatch`, 'DESC');
    // qb.orderBy(`nameFilter.name = :qClean`, 'DESC');

    // qb.where('nameFilter.name LIKE(:q)', { q });
    // .innerJoin('node.names', 'nameFilter', 'nameFilter.name LIKE :q', {
    //   q: `${q}`,
    // });

    const filterDivisionNames = (
      Array.isArray(taxNodeFindOptions.divisionName)
        ? taxNodeFindOptions.divisionName
        : [taxNodeFindOptions.divisionName]
    ).filter((e) => e);

    // console.log('toto', divisionNames, divisionNames.length);

    if (filterDivisionNames.length) {
      qb.innerJoin(
        'node.division',
        'divisionFilter',
        'divisionFilter.name IN(:...filterDivisionNames)',
        {
          filterDivisionNames,
        },
      );
    }

    const filterRanks = (
      Array.isArray(taxNodeFindOptions.rank)
        ? taxNodeFindOptions.rank
        : [taxNodeFindOptions.rank]
    ).filter((el) => el);

    if (filterRanks.length) {
      qb.andWhere('node.rank IN(:...filterRanks)', { filterRanks });
    }

    // .addSelect('LOWER(searchNames.name)', 'node_nameLowerCase')
    // .where('TRUE', { startsByQ: `${q}%`, q }); // dirty hack to pass variable TODO: there has to be a better way

    // qb.addOrderBy(`(node_nameLowerCase = :q)`, 'DESC');
    // qb.addOrderBy(`(node_nameLowerCase LIKE :startsByQ)`, 'DESC');
    // qb.addOrderBy(`"searchNames"."name"`, 'DESC');

    // for (const word of words) {
    //   qb.andWhere('(LOWER(searchNames.name) LIKE :word)', {
    //     word: `%${word}%`,
    //   });
    // }
    // qb.where('searchNames.name LIKE :q', );

    /**
     * inject taxNodeFindOptions.joins related models to SelectQueryBuilder
     */

    const total = await qb.getCount();

    await this.qbAddJoins(qb, taxNodeFindOptions.join);

    // Paginated

    const { limit, offset } = paginatedOptionsDto;

    qb.take(limit).skip(offset);

    let nodes = await qb.getMany();

    // console.log('join', taxNodeFindOptions.join);
    // console.log('findByName', paginatedOptionsDto, total);
    // console.log('nodesCount', nodes.length);
    /**
     * recursively fetches node.parent.parent and/or node.parentFlatAncestors
     */

    await this.nodesAddParentAncestors(nodes, taxNodeFindOptions);

    const paginatedMetaDto = new PaginatedMetaDto({
      total,
      paginatedOptionsDto,
    });

    return new PaginatedDto(nodes, paginatedMetaDto);
  }

  // async findByNameTrash(
  //   q: string,
  //   findOptions: TaxNodeFindOptions = taxNodeFindOptionsDefault,
  // ): Promise<TaxNode[]> {
  //   const words = q.toLowerCase().split(/\s+/g);
  //   q = words.join(' ');

  //   const qb = this.taxNodeRepository
  //     .createQueryBuilder('node')
  //     .select('node.id')
  //     .addSelect('LOWER(names.name)', 'nameLowerCase')
  //     .leftJoin('node.names', 'names')
  //     .where('TRUE', { startsByQ: `${q}%` });

  //   for (const word of words) {
  //     qb.andWhere('LOWER(names.name) LIKE :word', { word: `%${word}%` });
  //   }

  //   // qb.addOrderBy(`nameLowerCase LIKE :startsByQ`, 'DESC');

  //   qb.take(10);

  //   const found = await qb.getMany();

  //   console.log('findByName', words, found);
  //   const ids = found.map((node) => node.id);

  //   return this.findByIds(ids, findOptions);
  // }

  async findByIds(
    id: StringOrNumber | StringOrNumber[],
    taxNodeFindOptions: TaxNodeFindOptions = taxNodeFindOptionsDefault,
  ): Promise<TaxNode[]> {
    /**
     * Accept 1234 | '345,456' | [1234, '2345', '345,456']
     */
    const ids: Number[] = (Array.isArray(id) ? id : [id])
      .join(',')
      .split(',')
      .map(Number);

    const qb = this.taxNodeRepository
      .createQueryBuilder('node')
      .select()
      .where({ id: In(ids) });

    /**
     * inject taxNodeFindOptions.joins related models to SelectQueryBuilder
     */

    await this.qbAddJoins(qb, taxNodeFindOptions.join);

    let nodes = await qb.getMany();

    /**
     * recursively fetches node.parent.parent and/or node.parentFlatAncestors
     */

    await this.nodesAddParentAncestors(nodes, taxNodeFindOptions);

    return nodes;
  }

  /**
   * Injects joins to QueryBuilder
   *
   * @param qb SelectQueryBuilder<TaxNode>
   * @param joins TaxNodeJoin[]
   */
  async qbAddJoins(
    qb: SelectQueryBuilder<TaxNode>,
    join: TaxNodeJoin | TaxNodeJoin[],
  ) {
    const joins = Array.isArray(join) ? join : [join];

    // qb.leftJoin('node.children', 'hasChildren');
    // .addSelect(
    //   'nodeHasChildren.id IS NULL',
    //   'isLeaf',
    // );

    qb.addSelect(
      'NOT EXISTS(SELECT "nodeIsLeaf".id FROM taxNode "nodeIsLeaf" WHERE "nodeIsLeaf"."parentId" = node.id)',
      'node_isLeaf',
    );

    if (joins.includes(TaxNodeJoin.names))
      qb.leftJoin('node.names', 'names').addSelect([
        'names.name',
        'names.nameUnique',
        'names.class',
        'names.id',
      ]);
    // .addOrderBy('names.class', 'ASC')
    // .addOrderBy('names.name', 'ASC');

    if (joins.includes(TaxNodeJoin.commonNames)) {
      qb.leftJoinAndMapMany(
        'node.commonNames',
        'node.names',
        'commonNames',
        // `commonNames.class = '${TaxNameClass.COMMON_NAME}'`,
        'commonNames.class = :commonNameClass',
        { commonNameClass: TaxNameClass.COMMON_NAME },
      );
    }

    if (joins.includes(TaxNodeJoin.scientificNames))
      qb.leftJoinAndMapMany(
        'node.scientificNames',
        'node.names',
        'scientificNames',
        "scientificNames.class = 'scientific name'",
      );

    if (joins.includes(TaxNodeJoin.division))
      qb.leftJoinAndSelect('node.division', 'division');

    if (joins.includes(TaxNodeJoin.genCode))
      qb.leftJoinAndSelect('node.genCode', 'genCode');

    if (joins.includes(TaxNodeJoin.mitochondrialGenCode))
      qb.leftJoinAndSelect('node.mitochondrialGenCode', 'mitochondrialGenCode');

    if (joins.includes(TaxNodeJoin.parent)) {
      qb.leftJoinAndSelect('node.parent', 'parent', 'parent.id != node.id');

      qb.addSelect('FALSE', 'parent_isLeaf');

      if (joins.includes(TaxNodeJoin.parentNames))
        qb.leftJoin('parent.names', 'parentNames').addSelect([
          'parentNames.name',
          'parentNames.nameUnique',
          'parentNames.class',
          'parentNames.id',
        ]);
      // .addOrderBy('parentNames.class', 'ASC')
      // .addOrderBy('parentNames.name', 'ASC');
    }

    /**
     * Children
     */

    if (joins.includes(TaxNodeJoin.children)) {
      qb.leftJoinAndSelect(
        'node.children',
        'children',
        'children.id != node.id', // exclude parent from children
      );

      // qb.addSelect(
      //   'EXISTS(SELECT n.id FROM taxNode n WHERE n.parentId = children.id)',
      //   'children_isLeaf',
      // );

      qb.addSelect(
        'NOT EXISTS(SELECT "childIsLeaf".id FROM taxNode "childIsLeaf" WHERE "childIsLeaf"."parentId" = children.id)',
        'children_isLeaf',
      );

      if (joins.includes(TaxNodeJoin.childrenNames))
        qb.leftJoin('children.names', 'childrenNames')
          .addSelect([
            'childrenNames.name',
            'childrenNames.nameUnique',
            'childrenNames.class',
            'childrenNames.id',
          ])
          .addOrderBy('childrenNames.class', 'ASC')
          .addOrderBy('childrenNames.name', 'ASC');

      if (joins.includes(TaxNodeJoin.childrenCommonNames))
        qb.leftJoinAndMapMany(
          'children.commonNames',
          'children.names',
          'childrenCommonNames',
          // "childrenCommonNames.class = 'common name'",
          'childrenCommonNames.class = :childrenCommonNamesClass',
          { childrenCommonNamesClass: TaxNameClass.COMMON_NAME },
        );

      if (joins.includes(TaxNodeJoin.childrenScientificNames))
        qb.leftJoinAndMapMany(
          'children.scientificNames',
          'children.names',
          'childrenScientificNames',
          "childrenScientificNames.class = 'scientific name'",
        );

      if (joins.includes(TaxNodeJoin.childrenDivision))
        qb.leftJoinAndSelect('children.division', 'childrenDivision');

      if (joins.includes(TaxNodeJoin.childrenGenCode))
        qb.leftJoinAndSelect('children.genCode', 'childrenGenCode');

      if (joins.includes(TaxNodeJoin.childrenMitochondrialGenCode))
        qb.leftJoinAndSelect(
          'children.mitochondrialGenCode',
          'childrenMitochondrialGenCode',
        );
    }
  }

  /**
   * Requires TaxNodeJoin.parent TaxNodeFindOptions.joins
   * TaxNodeJoin.parentAncestors: recursively creates parent.parent
   * TaxNodeJoin.parentFlatAncestors: fils in node.parentFlatAncestors
   *
   * @param nodes TaxNode[]
   * @param taxNodeFindOptions TaxNodeFindOptions.joins
   * @returns TaxNode[]
   */
  async nodesAddParentAncestors(
    nodes: TaxNode[],
    taxNodeFindOptions: TaxNodeFindOptions = taxNodeFindOptionsDefault,
  ): Promise<TaxNode[]> {
    const { ancestorsLimitDepth } = taxNodeFindOptions;

    const joins = Array.isArray(taxNodeFindOptions.join)
      ? taxNodeFindOptions.join
      : [taxNodeFindOptions.join];

    const ancestorsLimitRank = Array.isArray(
      taxNodeFindOptions.ancestorsLimitRank,
    )
      ? taxNodeFindOptions.ancestorsLimitRank
      : [taxNodeFindOptions.ancestorsLimitRank];

    for (let node of nodes) {
      if (
        joins.includes(TaxNodeJoin.parent) &&
        (joins.includes(TaxNodeJoin.parentAncestors) ||
          joins.includes(TaxNodeJoin.parentFlatAncestors)) &&
        node &&
        node.parent &&
        node.parent.id != rootTaxNodeId &&
        !(
          ancestorsLimitRank &&
          [node.rank, node.parent.rank].some((e) =>
            ancestorsLimitRank.includes(e),
          )
        ) // if node.rank or parent.rank is of ancestorsLimitRank
      ) {
        let ancestor = JSON.parse(JSON.stringify(node.parent)); // clone
        let tree = ancestor;
        let parentFlatAncestors = []; // [JSON.parse(JSON.stringify(result.parent))];
        let ancestorDepth = 0;

        while (true) {
          ancestorDepth++;

          // if (ancestor && ancestor.id != rootTaxNodeId) {
          if (
            !ancestor ||
            ancestor.id == rootTaxNodeId || //reached root
            (ancestorsLimitDepth > 0 &&
              ancestorsLimitDepth <= ancestorDepth - 1) ||
            (ancestorsLimitRank && ancestorsLimitRank.includes(ancestor.rank))
          ) {
            if (joins.includes(TaxNodeJoin.parentAncestors)) {
              node.parent.parent = tree.parent;
            }

            if (joins.includes(TaxNodeJoin.parentFlatAncestors)) {
              node.parentFlatAncestors = parentFlatAncestors;
            }

            break;
          } else {
            const qbAncestorParent = this.taxNodeRepository
              .createQueryBuilder('node')
              .select()
              .where({ id: ancestor.parentId });

            if (joins.includes(TaxNodeJoin.parentAncestorsNames)) {
              qbAncestorParent
                .leftJoin('node.names', 'names')
                .addSelect([
                  'names.name',
                  'names.nameUnique',
                  'names.class',
                  'names.id',
                ])
                .addOrderBy('names.class', 'ASC')
                .addOrderBy('names.name', 'ASC');
            }

            const ancestorParent = await qbAncestorParent.getOne();

            if (joins.includes(TaxNodeJoin.parentFlatAncestors)) {
              parentFlatAncestors.push(
                JSON.parse(JSON.stringify(ancestorParent)),
              );
            }

            // assign
            ancestor.parent = ancestorParent;

            // move cursor/pointer
            ancestor = ancestor.parent;
          }
        }
      } else {
        if (joins.includes(TaxNodeJoin.parentFlatAncestors)) {
          node.parentFlatAncestors = [];
        }
      }
    }

    return nodes;
  }

  // create(createTaxNodeDto: CreateTaxNodeDto) {
  //   return 'This action adds a new taxNode';
  // }

  // findAll() {
  //   return `This action returns all taxNode`;
  // }

  // update(id: number, updateTaxNodeDto: UpdateTaxNodeDto) {
  //   return `This action updates a #${id} taxNode`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} taxNode`;
  // }
}
