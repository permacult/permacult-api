/**
 * @description Replace accentuated characters with their ASCII equivalent
 *              by default
 *                - removes utf8 spacial spacers (normalizeSpaces)
 *                - trims the string (normalizeSpaces)
 *
 * @param str string required
 * @param normalizeSpaces boolean remplaces repeated \n\r\t = \s
 * @param trim RegExp | false - default: /^\s+|\s+$/
 *             To trim or not to trim, that is the question
 * @returns string
 *
 * @version 0.0.1
 *
 * @author Antony GIBBS <antony@cantoute.com>
 * @licence none - free to use - please update me of any fix or improvement.
 *
 * Changelog:
 *  0.0.1
 *    - Added param normalizeSpaces and trim - 2022.08.22
 *
 */

export const stripAccents = (
  str: string,
  normalizeSpaces: boolean = true,
  trim: RegExp | false = /^\s+|\s+$/,
): string => {
  str = str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

  if (normalizeSpaces) str = str.replace(/\s+/, ' ');

  return trim === false ? str : str.replace(trim, '');
};
