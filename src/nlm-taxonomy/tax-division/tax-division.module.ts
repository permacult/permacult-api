import { Module } from '@nestjs/common';
import { TaxDivisionService } from './tax-division.service';
import { TaxDivisionController } from './tax-division.controller';

@Module({
  controllers: [TaxDivisionController],
  providers: [TaxDivisionService]
})
export class TaxDivisionModule {}
