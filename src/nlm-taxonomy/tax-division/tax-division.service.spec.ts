import { Test, TestingModule } from '@nestjs/testing';
import { TaxDivisionService } from './tax-division.service';

describe('TaxDivisionService', () => {
  let service: TaxDivisionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaxDivisionService],
    }).compile();

    service = module.get<TaxDivisionService>(TaxDivisionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
