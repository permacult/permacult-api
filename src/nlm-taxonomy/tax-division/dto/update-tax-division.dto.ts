import { PartialType } from '@nestjs/swagger';
import { CreateTaxDivisionDto } from './create-tax-division.dto';

export class UpdateTaxDivisionDto extends PartialType(CreateTaxDivisionDto) {}
