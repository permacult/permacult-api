import { Injectable } from '@nestjs/common';
import { CreateTaxDivisionDto } from './dto/create-tax-division.dto';
import { UpdateTaxDivisionDto } from './dto/update-tax-division.dto';

@Injectable()
export class TaxDivisionService {
  create(createTaxDivisionDto: CreateTaxDivisionDto) {
    return 'This action adds a new taxDivision';
  }

  findAll() {
    return `This action returns all taxDivision`;
  }

  findOne(id: number) {
    return `This action returns a #${id} taxDivision`;
  }

  update(id: number, updateTaxDivisionDto: UpdateTaxDivisionDto) {
    return `This action updates a #${id} taxDivision`;
  }

  remove(id: number) {
    return `This action removes a #${id} taxDivision`;
  }
}
