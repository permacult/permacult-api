import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TaxDivisionService } from './tax-division.service';
import { CreateTaxDivisionDto } from './dto/create-tax-division.dto';
import { UpdateTaxDivisionDto } from './dto/update-tax-division.dto';
import { NLM_API_BASE } from 'src/shared/constants';

@Controller([NLM_API_BASE, 'tax-division'].join('/'))
export class TaxDivisionController {
  constructor(private readonly taxDivisionService: TaxDivisionService) {}

  // @Post()
  // create(@Body() createTaxDivisionDto: CreateTaxDivisionDto) {
  //   return this.taxDivisionService.create(createTaxDivisionDto);
  // }

  // @Get()
  // findAll() {
  //   return this.taxDivisionService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.taxDivisionService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateTaxDivisionDto: UpdateTaxDivisionDto,
  // ) {
  //   return this.taxDivisionService.update(+id, updateTaxDivisionDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.taxDivisionService.remove(+id);
  // }
}
