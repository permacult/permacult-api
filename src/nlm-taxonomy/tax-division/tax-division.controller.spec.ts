import { Test, TestingModule } from '@nestjs/testing';
import { TaxDivisionController } from './tax-division.controller';
import { TaxDivisionService } from './tax-division.service';

describe('TaxDivisionController', () => {
  let controller: TaxDivisionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaxDivisionController],
      providers: [TaxDivisionService],
    }).compile();

    controller = module.get<TaxDivisionController>(TaxDivisionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
