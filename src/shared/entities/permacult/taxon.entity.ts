import { TimestampedEntity } from 'src/shared/entity-default';
import { Rank } from 'src/shared/gbif/enums/rank.enum';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

// @Index('taxon_pkey', ['id'], { unique: true })
@Index('idx_taxon_scientificName', ['scientificName'], {})
@Index('idx_taxon_canonicalName', ['canonicalName'], {})
@Index('idx_taxon_rank', ['rank'], {})
@Index('idx_taxon_genus', ['genus'], {})
@Entity('taxon', { schema: 'public' })
export class Taxon extends TimestampedEntity {
  @Column({ type: 'integer', primary: true })
  id: number;

  @Column({ type: 'uuid', nullable: true })
  datasetId: string | null;

  @Column({ type: 'integer', nullable: true })
  parentNameUsageId: number | null;

  @Column({ type: 'integer', nullable: true })
  acceptedNameUsageId: number | null;

  @Column({ type: 'integer', nullable: true })
  originalNameUsageId: number | null;

  @Column({
    type: 'varchar',
    length: 512,
    collation: 'NOCASE',
  })
  scientificName: string;

  @Column('varchar', {
    nullable: true,
    length: 512,
  })
  scientificNameAuthorship: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
    collation: 'NOCASE',
  })
  canonicalName: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
    collation: 'NOCASE',
  })
  genericName: string | null;

  @Column({ type: 'text', nullable: true })
  specificEpithet: string | null;

  @Column({
    type: 'varchar',
    name: 'infraspecificEpithet',
    nullable: true,
    length: 512,
  })
  infraspecificEpithet: string | null;

  @Column({
    type: 'simple-enum',
    enum: Rank,
    enumName: 'Rank',
    // nullable: true,
    default: Rank.UNRANKED,
  })
  rank: Rank;

  @Column({
    type: 'varchar',
    nullable: true,
    collation: 'NOCASE',
  })
  nameAccordingTo: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    collation: 'NOCASE',
  })
  namePublishedIn: string | null;

  @Column('varchar', { name: 'status', nullable: true, length: 512 })
  status: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 512,
  })
  nomenclaturalStatus: string | null;

  @Column({ type: 'text', nullable: true })
  remarks: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
  })
  kingdomName: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
  })
  phylumName: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
  })
  className: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
  })
  orderName: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
  })
  familyName: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 128,
  })
  genusName: string | null;

  @Column({ type: 'integer', nullable: true })
  kingdomId: number | null;

  @Column({ type: 'integer', nullable: true })
  phylumId: number | null;

  @Column({ type: 'integer', nullable: true })
  classId: number | null;

  @Column({ type: 'integer', nullable: true })
  orderId: number | null;

  @Column({ type: 'integer', nullable: true })
  familyId: number | null;

  @Column({ type: 'integer', nullable: true })
  genusId: number | null;

  @ManyToOne(() => Taxon, (taxon) => taxon.classes, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'classId', referencedColumnName: 'id' }])
  class?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.class)
  classes?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.families, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'familyId', referencedColumnName: 'id' }])
  family?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.family)
  families?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.genera, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'genusId', referencedColumnName: 'id' }])
  genus?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.genus)
  genera?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.kingdoms, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'kingdomId', referencedColumnName: 'id' }])
  kingdom?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.kingdom)
  kingdoms?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.phyla, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'phylumId', referencedColumnName: 'id' }])
  phylum?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.phylum)
  phyla?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.parentNameUsages, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'acceptedParentUsageId', referencedColumnName: 'id' }])
  parentNameUsage?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.parentNameUsage)
  parentNameUsages?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.acceptedNameUsages, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'acceptedNameUsageId', referencedColumnName: 'id' }])
  acceptedNameUsage?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.acceptedNameUsage)
  acceptedNameUsages?: Taxon[];

  @ManyToOne(() => Taxon, (taxon) => taxon.originalNameUsages, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'originalNameUsageId', referencedColumnName: 'id' }])
  originalNameUsage?: Taxon;

  @OneToMany(() => Taxon, (taxon) => taxon.originalNameUsage)
  originalNameUsages?: Taxon[];
}
