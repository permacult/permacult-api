import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Tree,
} from 'typeorm';
import { SoftDeleteTimestampedEntity } from '../../entity-default';

export enum TaxonomyRank {
  DOMAIN = 'regio',
  KINGDOM = 'regnum',
  PHYLUM = 'phylum',
  CLASS = 'classis',
  ORDER = 'ordo',
  FAMILY = 'familia',
  GENUS = 'genus',
  // SUBGENUS = 'subgenus',
  SPECIES = 'species',
  // SUBSPECIES = 'subspecies'
}

/**
 *
biotype
clade
class
cohort
family
forma
forma specialis
genotype
genus
infraclass
infraorder
isolate
kingdom
morph
no rank
order
parvorder
pathogroup
phylum
section
series
serogroup
serotype
species
species group
species subgroup
strain
subclass
subcohort
subfamily
subgenus
subkingdom
suborder
subphylum
subsection
subspecies
subtribe
superclass
superfamily
superkingdom
superorder
superphylum
tribe
varietas

 */

@Entity({ name: 'taxonomy' })
// @Tree('closure-table', {
//   closureTableName: 'taxonomyClosure',
//   ancestorColumnName: (column) => 'ancestor_' + column.propertyName,
//   descendantColumnName: (column) => 'descendant_' + column.propertyName,
// })
export class Taxonomy extends SoftDeleteTimestampedEntity {
  // @ApiProperty({
  //   type: 'string',
  //   format: 'uuid',
  // })
  // @PrimaryGeneratedColumn('uuid')
  // id: string;

  @ApiProperty({
    type: Number,
    required: false,
  })
  @Column({
    type: 'int',
    generated: true,
    primary: true,
  })
  id?: number;

  @ApiProperty({
    type: String,
    example: 'Animalia',
    required: true,
  })
  @Column({
    type: 'varchar',
    length: 255,
  })
  @Index({
    unique: true,
  })
  name: string;

  @ApiProperty({
    type: 'enum',
    enum: TaxonomyRank,
  })
  @Column({
    type: 'simple-enum',
    enum: TaxonomyRank,
  })
  rank: TaxonomyRank;

  @ApiProperty()
  @Column({
    type: 'text',
    default: '',
  })
  description?: string;

  @Column({
    type: 'int',
    nullable: true,
  })
  parentId?: number;

  @ApiProperty({
    type: Taxonomy,
  })
  @ManyToOne((type) => Taxonomy, (taxonomy) => taxonomy.children, {
    onUpdate: 'CASCADE',
    onDelete: 'RESTRICT',
  })
  @JoinColumn({
    name: 'parentId',
  })
  parent?: Taxonomy;

  @ApiProperty({
    type: [Taxonomy],
  })
  @OneToMany((type) => Taxonomy, (taxonomy) => taxonomy.parent)
  children?: Taxonomy[];
}
