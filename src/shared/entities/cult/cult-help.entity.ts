import { TimestampedUpdatedEntity } from 'src/shared/entity-default';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { CultNode } from './cult-node.entity';

@Entity({ name: 'cult_help' })
export class CultHelp extends TimestampedUpdatedEntity {
  @Column({
    type: 'integer',
    primary: true,
  })
  cultNodeId: number;

  @Column({
    type: 'integer',
    primary: true,
  })
  helpsCultNodeId: number;

  descriptions;

  @ManyToOne(() => CultNode, (n) => n.helpers, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'helpsCultNodeId' })
  helper?: CultNode;

  @ManyToOne(() => CultNode, (n) => n.helps, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  helping?: CultNode;
}
