import { ApiProperty } from '@nestjs/swagger';
import { Taxon } from 'src/shared/entities/permacult/taxon.entity';
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { CultNode } from './cult-node.entity';

@Entity({ name: 'cult_node_taxon' })
export class CultNodeTaxon extends BaseEntity {
  @ApiProperty({
    type: Number,
  })
  @Column({
    type: 'integer',
    primary: true,
  })
  nodeId: number;

  @ApiProperty({
    type: Number,
  })
  @Column({
    type: 'integer',
    primary: true,
  })
  gbifID: number; // aka taxonKey or /v1/species/:id

  @ManyToOne(() => CultNode, (node) => node.nodeTaxons, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'gbifID',
    referencedColumnName: 'id',
  })
  node: CultNode;

  // @ManyToOne(() => Taxon, {
  //   onDelete: 'RESTRICT',
  //   onUpdate: 'CASCADE',
  // })
  // @JoinColumn({
  //   name: 'taxonId',
  //   referencedColumnName: 'id',
  // })
  // taxon: Taxon;
}
