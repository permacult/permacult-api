import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength } from 'class-validator';
import { SoftDeleteTimestampedEntity } from 'src/shared/entity-default';
import {
  Column,
  Entity,
  Index,
  ManyToMany,
  OneToMany,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { CultHelp } from './cult-help.entity';
import { CultNodeTaxon } from './cult-node-taxon.entity';

export enum Rank {
  UNRANKED = 'UNRANKED',
  DOMAIN = 'REGIO',
  KINGDOM = 'REGNUM',
  PHYLUM = 'PHYLUM',
  CLASS = 'CLASSIS',
  ORDER = 'ORDO',
  FAMILY = 'FAMILIA',
  GENUS = 'GENUS',
  SUBGENUS = 'SUBGENUS',
  SPECIES = 'SPECIES',
  SUBSPECIES = 'SUBSPECIES',
  VARIETY = 'VARIETAS',
  CULTIVAR = 'CULTIVAR', // plant cultivated variety
}

@Entity({ name: 'cult_node' })
// @Tree('closure-table', {
//   closureTableName: 'node-closure',
//   ancestorColumnName: (column) => 'ancestor_' + column.propertyName,
//   descendantColumnName: (column) => 'descendant_' + column.propertyName,
// })

@Tree('materialized-path')
@Index('idx_cult_node_commonName', ['commonName'], { unique: true })
@Index('idx_cult_node_scientificName', ['scientificName'], { unique: true })
export class CultNode extends SoftDeleteTimestampedEntity {
  @ApiProperty({
    type: Number,
    required: false,
  })
  @Column({
    type: 'integer',
    generated: true,
    primary: true,
  })
  id: number;

  @IsNotEmpty()
  @MinLength(3)
  @ApiProperty({
    type: String,
    example: 'Tomatoes',
    required: true,
    description: 'Common name in english en_US',
  })
  @Column({
    type: 'varchar',
    length: 255,
    collation: 'NOCASE',
  })
  // @Index({
  //   unique: true,
  // })
  commonName: string;

  @ApiProperty({
    type: String,
    example: 'Solanum lycopersicum L.',
    nullable: true,
  })
  @Column({
    type: 'varchar',
    length: 255,
    collation: 'NOCASE',
    nullable: true,
    default: null,
  })
  scientificName: string;

  @Column({
    type: 'simple-enum',
    enum: Rank,
    enumName: 'Rank',
    // nullable: true,
    default: Rank.UNRANKED,
  })
  rank: Rank;

  // @ApiProperty({
  //   required: false,
  //   default: false,
  // })
  // @Column({
  //   type: Boolean,
  //   default: false,
  // })
  // isSynonym: boolean;

  @ApiProperty({
    required: false,
  })
  @Column({
    type: 'text',
    default: '',
  })
  description: string;

  @ApiProperty({
    required: false,
    nullable: true,
  })
  @Column({
    type: 'text',
    nullable: true,
  })
  remarks: string | null;

  @OneToMany(() => CultNodeTaxon, (nodeTaxon) => nodeTaxon.node)
  nodeTaxons?: CultNodeTaxon[];

  @TreeChildren()
  children?: CultNode[];

  @TreeParent()
  parent?: CultNode;

  @OneToMany(() => CultHelp, (help) => help.helper, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  helpHelpers?: CultHelp[];

  @OneToMany(() => CultHelp, (help) => help.helping, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  helpHelps?: CultHelp[];

  @ManyToMany(() => CultNode, (node) => node.helpers, {
    cascade: true,
  })
  helps: CultNode[];

  @ManyToMany(() => CultNode, (node) => node.helps)
  helpers: CultNode[];
}
