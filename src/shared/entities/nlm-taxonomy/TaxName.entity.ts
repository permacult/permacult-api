import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { TaxNode } from './TaxNode.entity';

export enum TaxNameClass {
  COMMON_NAME = 'common name',
  EQUIVALENT_NAME = 'equivalent name',
  GENBANK_ACRONYM = 'genbank acronym',
  GENBANK_COMMON_NAME = 'genbank common name',
  SCIENTIFIC_NAME = 'scientific name',
  SYNONYM = 'synonym',
  INCLUDES = 'includes',
  AUTHORITY = 'authority',
  ACRONYM = 'acronym',
  TYPE_MATERIAL = 'type material',
  IN_PART = 'in-part',
  BLAST_NAME = 'blast name',
}

// @Index('taxName_pkey', ['id'], { unique: true })
@Index('fki_lnk_taxNode_taxName', ['taxNodeId'], {})
@Index('taxName_taxNodeId_idx', ['taxNodeId'], {})
@Entity('taxName')
export class TaxName extends BaseEntity {
  @ApiProperty()
  @Column({ type: 'integer' })
  taxNodeId: number;

  @ApiProperty()
  @Column({ type: 'text' })
  name: string;

  @ApiProperty()
  @Column({
    type: 'text',
    nullable: true,
  })
  nameUnique: string | null;

  @ApiProperty({
    nullable: true,
    enum: TaxNameClass,
  })
  @Column({
    type: 'simple-enum',
    enum: TaxNameClass,
    nullable: true,
  })
  class: TaxNameClass | null;

  @ApiProperty()
  @Column({
    type: 'integer',
    primary: true,
    generated: 'increment',
  })
  id: number;

  @ApiProperty({
    type: () => TaxNode,
    required: false,
  })
  @ManyToOne(() => TaxNode, (taxNode) => taxNode.names, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'taxNodeId', referencedColumnName: 'id' }])
  taxNode?: TaxNode;
}
