import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { TaxName } from './TaxName.entity';
import { GenCode } from './GenCode.entity';
import { TaxDivision } from './TaxDivision.entity';
import { ApiProperty } from '@nestjs/swagger';

// import { TaxonomyRank } from 'src/shared/entities/taxonomy.entity';

// TODO: put in right order
export enum TaxNodeRank {
  NO_RANK = 'no rank',
  BIOTYPE = 'biotype',
  SUPERKINGDOM = 'superkingdom',
  KINGDOM = 'kingdom',
  SUBKINGDOM = 'subkingdom',
  SUPERPHYLUM = 'superphylum',
  PHYLUM = 'phylum',
  SUBPHYLUM = 'subphylum',
  SUPERCLASS = 'superclass',
  CLASS = 'class',
  SUBCLASS = 'subclass',
  INFRACLASS = 'infraclass',
  SUPERORDER = 'superorder',
  ORDER = 'order',
  SUBORDER = 'suborder',
  INFRAORDER = 'infraorder',
  SUPERFAMILY = 'superfamily',
  FAMILY = 'family',
  SUBFAMILY = 'subfamily',
  GENUS = 'genus',
  SUBGENUS = 'subgenus',
  SPECIES = 'species',
  SUBSPECIES = 'subspecies',
  SPECIES_GROUP = 'species group',
  SPECIES_SUBGROUP = 'species subgroup',

  COHORT = 'cohort',
  SUBCOHORT = 'subcohort',
  TRIBE = 'tribe',
  SUBTRIBE = 'subtribe',
  CLADE = 'clade',
  FORMA = 'forma',
  FORMA_SPECIALIST = 'forma specialis',
  GENOTYPE = 'genotype',
  ISOLATE = 'isolate',
  MORPH = 'morph',
  PARVORDER = 'parvorder',
  PATHGROUP = 'pathogroup',
  SECTION = 'section',
  SUBSECTION = 'subsection',
  SERIES = 'series',
  SEROGROUP = 'serogroup',
  SEROTYPE = 'serotype',
  STRAIN = 'strain',
  VARIETAS = 'varietas',
}

@Index('taxNode_genCode_idx', ['genCodeId'], {})
@Index('taxNode_mitochondrialGenCode_idx', ['mitochondrialGenCodeId'], {})
// @Index('taxNode_pkey', ['id'], { unique: true })
@Index('taxNode_parentId_idx', ['parentId'], {})
@Index('taxNode_taxDivisionId_idx', ['taxDivisionId'], {})
@Entity('taxNode')
export class TaxNode extends BaseEntity {
  @ApiProperty()
  @Column({
    type: 'integer',
    primary: true,
    nullable: false,
  })
  id: number;

  @ApiProperty()
  @Column({
    type: 'integer',
    nullable: false,
  })
  parentId: number;

  @ApiProperty({
    type: 'simple-enum',
    enum: TaxNodeRank,
    default: TaxNodeRank.NO_RANK,
    // enumName: 'TaxNodeRank',
  })
  @Column({
    type: 'simple-enum',
    enum: TaxNodeRank,
    default: TaxNodeRank.NO_RANK,
    enumName: 'TaxNodeRank',
  })
  rank: TaxNodeRank;

  @ApiProperty()
  @Column({
    type: 'text',
    nullable: true,
  })
  emblCode: string | null;

  @ApiProperty()
  @Column({
    type: 'integer',
    nullable: false,
  })
  taxDivisionId: number;

  @ApiProperty()
  @Column({
    type: 'boolean',
    nullable: false,
    default: () => false,
  })
  inheritedDivisionFlag: boolean;

  @ApiProperty()
  @Column({ type: 'integer' })
  genCodeId: number;

  @ApiProperty()
  @Column({
    type: 'boolean',
    nullable: false,
    default: () => false,
  })
  inheritedGenCodeFlag: boolean;

  @ApiProperty()
  @Column({ type: 'integer' })
  mitochondrialGenCodeId: number;

  @ApiProperty()
  @Column({
    type: 'boolean',
    nullable: false,
    default: () => false,
  })
  inheritedMitochondrialGenCodeFlag: boolean;

  @ApiProperty()
  @Column({
    type: 'boolean',
    nullable: false,
    default: () => false,
  })
  genBankHiddenFlag: boolean;

  @ApiProperty()
  @Column({
    type: 'boolean',
    nullable: false,
    default: () => false,
  })
  hiddenSubtreeRootFlag: boolean;

  @ApiProperty()
  @Column({
    type: 'text',
    nullable: true,
  })
  comment: string | null;

  @ApiProperty({
    isArray: true,
    type: () => TaxName,
  })
  @OneToMany(() => TaxName, (taxName) => taxName.taxNode)
  names?: TaxName[];

  @ApiProperty({
    type: () => GenCode,
  })
  @ManyToOne(() => GenCode, (genCode) => genCode.taxNodes, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'genCodeId', referencedColumnName: 'id' }])
  genCode?: GenCode;

  @ApiProperty({
    type: () => GenCode,
  })
  @ManyToOne(() => GenCode, (genCode) => genCode.mitochondrialTaxNodes, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'mitochondrialGenCodeId', referencedColumnName: 'id' }])
  mitochondrialGenCode?: GenCode;

  @ApiProperty({
    example: '<TaxNode>',
  })
  @ManyToOne(() => TaxNode, (taxNode) => taxNode.children, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'parentId', referencedColumnName: 'id' }])
  parent?: TaxNode;

  @ApiProperty({
    type: () => TaxNode,
    isArray: true,
    example: ['<TaxNode>'],
  })
  @OneToMany(() => TaxNode, (taxNode) => taxNode.parent)
  children?: TaxNode[];

  @ApiProperty({
    type: () => TaxDivision,
  })
  @ManyToOne(() => TaxDivision, (taxDivision) => taxDivision.nodes, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn([{ name: 'taxDivisionId', referencedColumnName: 'id' }])
  division?: TaxDivision;

  @ApiProperty({
    name: 'commonNames?',
    type: () => TaxName,
    isArray: true,
  })
  commonNames?: TaxName[];

  @ApiProperty({
    type: () => TaxName,
    isArray: true,
  })
  scientificNames?: TaxName[];

  @ApiProperty({
    type: () => TaxNode,
    isArray: true,
  })
  parentFlatAncestors?: TaxNode[];

  @ApiProperty({
    type: Boolean,
    required: false,
  })
  @Column({ select: false })
  isLeaf?: Boolean | null | undefined;
}
