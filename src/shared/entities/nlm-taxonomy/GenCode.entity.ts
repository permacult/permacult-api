import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity, Column, Entity, Index, OneToMany } from 'typeorm';
import { TaxNode } from './TaxNode.entity';

// @Index("genCode_pkey", ["id"], { unique: true })
@Entity('genCode')
export class GenCode extends BaseEntity {
  @ApiProperty()
  @Column({
    type: 'integer',
    primary: true,
  })
  id: number;

  @ApiProperty()
  @Column({
    type: 'text',
    nullable: true,
  })
  abbreviation: string | null;

  @ApiProperty()
  @Column({ type: 'text' })
  name: string;

  @ApiProperty({
    nullable: true,
  })
  @Column({
    type: 'text',
    nullable: true,
  })
  cde: string | null;

  @ApiProperty({
    nullable: true,
  })
  @Column({
    type: 'text',
    nullable: true,
  })
  starts: string | null;

  @ApiProperty({
    type: () => TaxNode,
    isArray: true,
  })
  @OneToMany(() => TaxNode, (taxNode) => taxNode.genCode)
  taxNodes: TaxNode[];

  @ApiProperty({
    type: () => TaxNode,
    isArray: true,
  })
  @OneToMany(() => TaxNode, (taxNode) => taxNode.mitochondrialGenCode)
  mitochondrialTaxNodes: TaxNode[];
}
