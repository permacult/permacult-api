import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity, Column, Entity, Index, OneToMany } from 'typeorm';
import { TaxNode } from './TaxNode.entity';

// @Index("taxDivision_pkey", ["id"], { unique: true })

export enum TaxDivisionName {
  BACTERIA = 'Bacteria',
  INVERTEBRATES = 'Invertebrates',
  MAMMALS = 'Mammals',
  PHAGES = 'Phages',
  PLANTS_AND_FUNGI = 'Plants and Fungi',
  PRIMATES = 'Primates',
  RODENTS = 'Rodents',
  SYNTHETIC_AND_CHIMERIC = 'Synthetic and Chimeric',
  UNASSIGNED = 'Unassigned',
  VIRUSES = 'Viruses',
  VERTEBRATES = 'Vertebrates',
  ENVIRONMENTAL_SAMPLES = 'Environmental samples',
}

@Entity('taxDivision')
export class TaxDivision extends BaseEntity {
  @ApiProperty()
  @Column({
    type: 'integer',
    primary: true,
  })
  id: number;

  @ApiProperty()
  @Column({ type: 'text' })
  code: string;

  @ApiProperty()
  @Column({
    type: 'text',
    nullable: true,
  })
  name: string | null;

  @ApiProperty()
  @Column({
    type: 'text',
    nullable: true,
  })
  comment: string | null;

  @ApiProperty({
    type: () => TaxNode,
    isArray: true,
  })
  @OneToMany(() => TaxNode, (taxNode) => taxNode.division)
  nodes: TaxNode[];
}
