import {
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

export abstract class TimestampedEntity extends BaseEntity {
  /**
   * Auto-generated at insert
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  // @CreateDateColumn({ type: 'timestamp with time zone' })
  @CreateDateColumn()
  createdAt?: Date;
}

export abstract class TimestampedUpdatedEntity extends BaseEntity {
  /**
   * Auto-updated at insert & update
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;
}

export abstract class SoftDeleteEntity extends BaseEntity {
  @DeleteDateColumn()
  deletedAt?: Date;
}

export abstract class SoftDeleteTimestampedEntity extends BaseEntity {
  @CreateDateColumn()
  createdAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}

export abstract class SoftDeleteTimestampedUpdatedEntity extends BaseEntity {
  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}
