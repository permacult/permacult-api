/**
 * @source https://gbif.github.io/gbif-api/apidocs/org/gbif/api/vocabulary/Rank.html
 */

export enum Rank {
  ABERRATION = 'aberration', // Zoological legacy rank

  BIOVAR = 'biovar',
  // Microbial rank based on biochemical or physiological properties.

  CHEMOFORM = 'chemoform',
  // Microbial infrasubspecific rank based on chemical constitution.

  CHEMOVAR = 'chemovar',
  // Microbial rank based on production or amount of production of a particular chemical.

  CLASS = 'class',
  COHORT = 'cohort',
  // Sometimes used in zoology, e.g.

  CONVARIETY = 'convariety',
  // A group of cultivars.

  CULTIVAR = 'cultivar',
  CULTIVAR_GROUP = 'cultivar group',
  // Rank in use from the code for cultivated plants.

  DOMAIN = 'domain',
  FAMILY = 'family',
  FORM = 'form',
  FORMA_SPECIALIS = 'forma specialis',
  // Microbial infrasubspecific rank.

  GENUS = 'genus',
  GRANDORDER = 'grandorder',
  GREX = 'grex',
  // The term grex has been coined to expand botanical nomenclature to describe hybrids of orchids.

  INFRACLASS = 'infraclass',
  INFRACOHORT = 'infracohort',
  INFRAFAMILY = 'infrafamily',
  INFRAGENERIC_NAME = 'infrageneric name',
  // used for any other unspecific rank below genera and above species.

  INFRAGENUS = 'infragenus',
  INFRAKINGDOM = 'infrakingdom',
  INFRALEGION = 'infralegion',
  INFRAORDER = 'infraorder',
  INFRAPHYLUM = 'infraphylum',
  INFRASPECIFIC_NAME = 'infraspecific name',
  // used for any other unspecific rank below species.

  INFRASUBSPECIFIC_NAME = 'infrasubspecific',
  // used also for any other unspecific rank below subspecies.

  INFRATRIBE = 'infratribe',
  KINGDOM = 'kingdom',
  LEGION = 'legion',
  // Sometimes used in zoology, e.g.

  MAGNORDER = 'magnorder',
  MORPH = 'morph',
  // Zoological legacy rank
  MORPHOVAR = 'morphovar',
  // Microbial rank based on morphological characteristics.

  NATIO = 'natio',
  // Zoological legacy rank
  ORDER = 'order',
  OTHER = 'other',
  // Any other rank we cannot map to this enumeration

  PARVCLASS = 'parvclass',
  PARVORDER = 'parvorder',
  PATHOVAR = 'pathovar',
  // Microbial rank based on pathogenic reactions in one or more hosts.

  PHAGOVAR = 'phagovar',
  // Microbial infrasubspecific rank based on reactions to bacteriophage.

  PHYLUM = 'phylum',
  PROLES = 'probes',
  // Botanical legacy rank

  RACE = 'race',
  // Botanical legacy rank

  SECTION = 'section',
  SERIES = 'series',
  SEROVAR = 'serovar',
  // Microbial infrasubspecific rank based on antigenic characteristics.

  SPECIES = 'species',
  SPECIES_AGGREGATE = 'species aggregate',
  // A loosely defined group of species.
  STRAIN = 'strain',
  // A microbial strain.
  SUBCLASS = 'subclass',
  SUBCOHORT = 'subcohort',
  SUBFAMILY = 'subfamily',
  SUBFORM = 'subform',
  SUBGENUS = 'subgenus',
  SUBKINGDOM = 'subkingdom',
  SUBLEGION = 'sublegion',
  SUBORDER = 'suborder',
  SUBPHYLUM = 'subphylum',
  SUBSECTION = 'subsection',
  SUBSERIES = 'subseries',
  SUBSPECIES = 'subspecies',
  SUBTRIBE = 'subtribe',
  SUBVARIETY = 'subvariety',
  SUPERCLASS = 'superclass',
  SUPERCOHORT = 'supercohort',
  SUPERFAMILY = 'superfamily',
  SUPERKINGDOM = 'superkingdom',
  SUPERLEGION = 'superlegion',
  SUPERORDER = 'superorder',
  SUPERPHYLUM = 'superphylum',
  SUPERTRIBE = 'supertribe',
  SUPRAGENERIC_NAME = 'suprageneric name',
  // Used for any other unspecific rank above genera.
  TRIBE = 'tribe',
  UNRANKED = 'unranked',
  VARIETY = 'variety',
}
