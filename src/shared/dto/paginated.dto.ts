import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';
import { PaginatedMetaDto } from './paginated-meta.dto';

export class PaginatedDto<T> {
  @IsArray()
  @ApiProperty({ isArray: true })
  readonly data: T[];

  @ApiProperty({ type: () => PaginatedMetaDto })
  readonly meta: PaginatedMetaDto;

  constructor(data: T[], meta: PaginatedMetaDto) {
    this.meta = meta;
    this.data = data;
  }
}
