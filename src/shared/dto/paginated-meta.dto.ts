import { ApiProperty } from '@nestjs/swagger';
import {
  PaginatedOptionsDto,
  DEFAULT_PAGINATED_LiMIT,
} from './paginated-options.dto';

export interface PaginatedMetaDtoParameters {
  paginatedOptionsDto: PaginatedOptionsDto;
  total: number;
}

export class PaginatedMetaDto {
  @ApiProperty()
  readonly limit: number;

  @ApiProperty()
  readonly offset: number;

  @ApiProperty()
  readonly total: number;

  @ApiProperty()
  readonly endOfRecords: boolean;

  constructor({ paginatedOptionsDto, total }: PaginatedMetaDtoParameters) {
    this.limit = +paginatedOptionsDto.limit;
    this.offset = +paginatedOptionsDto.offset;
    this.total = +total;
    this.endOfRecords = this.offset + this.limit >= this.total;
  }
}
