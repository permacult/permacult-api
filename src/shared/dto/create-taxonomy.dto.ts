import { PartialType } from '@nestjs/swagger';
import { Taxonomy, TaxonomyRank } from '../entities/permacult/taxonomy.entity';

export class CreateTaxonomyDto {
  name: string;

  parentId?: number | null;
  parent: Taxonomy;

  rank: TaxonomyRank;
}
