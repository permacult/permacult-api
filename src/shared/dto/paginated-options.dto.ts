import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, Min, Max, IsOptional } from 'class-validator';

export const DEFAULT_PAGINATED_LiMIT = 10;

export const defaultPaginatedOptions: PaginatedOptionsDto = {
  limit: DEFAULT_PAGINATED_LiMIT,
  offset: 0,
};

export class PaginatedOptionsDto {
  @ApiProperty({
    minimum: 1,
    maximum: 500,
    // default: 10,
    required: false,
  })
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @Max(500)
  readonly limit?: number = DEFAULT_PAGINATED_LiMIT;

  @ApiProperty({
    minimum: 0,
    // default: 0,
    required: false,
  })
  @Type(() => Number)
  @IsInt()
  @Min(0)
  readonly offset?: number = 0;
}
