import { Module } from '@nestjs/common';
import { CultHelpsModule } from './cult-helps/cult-helps.module';
import { CultNodeTaxonsModule } from './cult-node-taxons/cult-node-taxons.module';
import { CultNodesModule } from './cult-nodes/cult-nodes.module';

@Module({
  imports: [CultNodesModule, CultNodeTaxonsModule, CultHelpsModule],
})
export class CultModule {}
