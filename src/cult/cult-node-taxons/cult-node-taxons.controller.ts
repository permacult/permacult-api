import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CultNodeTaxonsService } from './cult-node-taxons.service';
import { CreateCultNodeTaxonDto } from './dto/create-cult-node-taxon.dto';
import { UpdateCultNodeTaxonDto } from './dto/update-cult-node-taxon.dto';

@Controller('cult-node-taxons')
export class CultNodeTaxonsController {
  constructor(private readonly cultNodeTaxonsService: CultNodeTaxonsService) {}

  @Post()
  create(@Body() createCultNodeTaxonDto: CreateCultNodeTaxonDto) {
    return this.cultNodeTaxonsService.create(createCultNodeTaxonDto);
  }

  @Get()
  findAll() {
    return this.cultNodeTaxonsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cultNodeTaxonsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCultNodeTaxonDto: UpdateCultNodeTaxonDto) {
    return this.cultNodeTaxonsService.update(+id, updateCultNodeTaxonDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cultNodeTaxonsService.remove(+id);
  }
}
