import { Injectable } from '@nestjs/common';
import { CreateCultNodeTaxonDto } from './dto/create-cult-node-taxon.dto';
import { UpdateCultNodeTaxonDto } from './dto/update-cult-node-taxon.dto';

@Injectable()
export class CultNodeTaxonsService {
  create(createCultNodeTaxonDto: CreateCultNodeTaxonDto) {
    return 'This action adds a new cultNodeTaxon';
  }

  findAll() {
    return `This action returns all cultNodeTaxons`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cultNodeTaxon`;
  }

  update(id: number, updateCultNodeTaxonDto: UpdateCultNodeTaxonDto) {
    return `This action updates a #${id} cultNodeTaxon`;
  }

  remove(id: number) {
    return `This action removes a #${id} cultNodeTaxon`;
  }
}
