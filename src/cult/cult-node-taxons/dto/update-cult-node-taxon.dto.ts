import { PartialType } from '@nestjs/swagger';
import { CreateCultNodeTaxonDto } from './create-cult-node-taxon.dto';

export class UpdateCultNodeTaxonDto extends PartialType(CreateCultNodeTaxonDto) {}
