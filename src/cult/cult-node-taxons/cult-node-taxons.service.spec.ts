import { Test, TestingModule } from '@nestjs/testing';
import { CultNodeTaxonsService } from './cult-node-taxons.service';

describe('CultNodeTaxonsService', () => {
  let service: CultNodeTaxonsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CultNodeTaxonsService],
    }).compile();

    service = module.get<CultNodeTaxonsService>(CultNodeTaxonsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
