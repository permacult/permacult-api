import { Test, TestingModule } from '@nestjs/testing';
import { CultNodeTaxonsController } from './cult-node-taxons.controller';
import { CultNodeTaxonsService } from './cult-node-taxons.service';

describe('CultNodeTaxonsController', () => {
  let controller: CultNodeTaxonsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CultNodeTaxonsController],
      providers: [CultNodeTaxonsService],
    }).compile();

    controller = module.get<CultNodeTaxonsController>(CultNodeTaxonsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
