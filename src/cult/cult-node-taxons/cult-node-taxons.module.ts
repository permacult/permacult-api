import { Module } from '@nestjs/common';
import { CultNodeTaxonsService } from './cult-node-taxons.service';
import { CultNodeTaxonsController } from './cult-node-taxons.controller';

@Module({
  controllers: [CultNodeTaxonsController],
  providers: [CultNodeTaxonsService],
})
export class CultNodeTaxonsModule {}
