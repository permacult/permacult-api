import { Test, TestingModule } from '@nestjs/testing';
import { CultNodesController } from './cult-nodes.controller';
import { CultNodesService } from './cult-nodes.service';

describe('CultNodesController', () => {
  let controller: CultNodesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CultNodesController],
      providers: [CultNodesService],
    }).compile();

    controller = module.get<CultNodesController>(CultNodesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
