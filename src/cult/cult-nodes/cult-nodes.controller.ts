import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CultNodesService } from './cult-nodes.service';
import { CreateCultNodeDto } from './dto/create-cult-node.dto';
import { UpdateCultNodeDto } from './dto/update-cult-node.dto';

@Controller('cult-nodes')
export class CultNodesController {
  constructor(private readonly cultNodesService: CultNodesService) {}

  @Post()
  create(@Body() createCultNodeDto: CreateCultNodeDto) {
    return this.cultNodesService.create(createCultNodeDto);
  }

  @Get()
  findAll() {
    return this.cultNodesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cultNodesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCultNodeDto: UpdateCultNodeDto) {
    return this.cultNodesService.update(+id, updateCultNodeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cultNodesService.remove(+id);
  }
}
