import { Injectable } from '@nestjs/common';
import { CreateCultNodeDto } from './dto/create-cult-node.dto';
import { UpdateCultNodeDto } from './dto/update-cult-node.dto';

@Injectable()
export class CultNodesService {
  create(createCultNodeDto: CreateCultNodeDto) {
    return 'This action adds a new cultNode';
  }

  findAll() {
    return `This action returns all cultNodes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cultNode`;
  }

  update(id: number, updateCultNodeDto: UpdateCultNodeDto) {
    return `This action updates a #${id} cultNode`;
  }

  remove(id: number) {
    return `This action removes a #${id} cultNode`;
  }
}
