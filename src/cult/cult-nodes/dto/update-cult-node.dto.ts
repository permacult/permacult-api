import { PartialType } from '@nestjs/swagger';
import { CreateCultNodeDto } from './create-cult-node.dto';

export class UpdateCultNodeDto extends PartialType(CreateCultNodeDto) {}
