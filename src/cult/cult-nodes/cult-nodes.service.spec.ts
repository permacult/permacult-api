import { Test, TestingModule } from '@nestjs/testing';
import { CultNodesService } from './cult-nodes.service';

describe('CultNodesService', () => {
  let service: CultNodesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CultNodesService],
    }).compile();

    service = module.get<CultNodesService>(CultNodesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
