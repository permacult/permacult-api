import { Module } from '@nestjs/common';
import { CultNodesService } from './cult-nodes.service';
import { CultNodesController } from './cult-nodes.controller';

@Module({
  controllers: [CultNodesController],
  providers: [CultNodesService],
})
export class CultNodesModule {}
