import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CultHelpsService } from './cult-helps.service';
import { CreateCultHelpDto } from './dto/create-cult-help.dto';
import { UpdateCultHelpDto } from './dto/update-cult-help.dto';

@Controller('cult-helps')
export class CultHelpsController {
  constructor(private readonly cultHelpsService: CultHelpsService) {}

  @Post()
  create(@Body() createCultHelpDto: CreateCultHelpDto) {
    return this.cultHelpsService.create(createCultHelpDto);
  }

  @Get()
  findAll() {
    return this.cultHelpsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cultHelpsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCultHelpDto: UpdateCultHelpDto) {
    return this.cultHelpsService.update(+id, updateCultHelpDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cultHelpsService.remove(+id);
  }
}
