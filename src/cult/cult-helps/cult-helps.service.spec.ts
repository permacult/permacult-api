import { Test, TestingModule } from '@nestjs/testing';
import { CultHelpsService } from './cult-helps.service';

describe('CultHelpsService', () => {
  let service: CultHelpsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CultHelpsService],
    }).compile();

    service = module.get<CultHelpsService>(CultHelpsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
