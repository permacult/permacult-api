import { Test, TestingModule } from '@nestjs/testing';
import { CultHelpsController } from './cult-helps.controller';
import { CultHelpsService } from './cult-helps.service';

describe('CultHelpsController', () => {
  let controller: CultHelpsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CultHelpsController],
      providers: [CultHelpsService],
    }).compile();

    controller = module.get<CultHelpsController>(CultHelpsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
