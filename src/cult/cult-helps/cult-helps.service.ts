import { Injectable } from '@nestjs/common';
import { CreateCultHelpDto } from './dto/create-cult-help.dto';
import { UpdateCultHelpDto } from './dto/update-cult-help.dto';

@Injectable()
export class CultHelpsService {
  create(createCultHelpDto: CreateCultHelpDto) {
    return 'This action adds a new cultHelp';
  }

  findAll() {
    return `This action returns all cultHelps`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cultHelp`;
  }

  update(id: number, updateCultHelpDto: UpdateCultHelpDto) {
    return `This action updates a #${id} cultHelp`;
  }

  remove(id: number) {
    return `This action removes a #${id} cultHelp`;
  }
}
