import { PartialType } from '@nestjs/swagger';
import { CreateCultHelpDto } from './create-cult-help.dto';

export class UpdateCultHelpDto extends PartialType(CreateCultHelpDto) {}
