import { Module } from '@nestjs/common';
import { CultHelpsService } from './cult-helps.service';
import { CultHelpsController } from './cult-helps.controller';

@Module({
  controllers: [CultHelpsController],
  providers: [CultHelpsService]
})
export class CultHelpsModule {}
