import { Module } from '@nestjs/common';
import { TaxonomiesService } from './taxonomies.service';
import { TaxonomiesController } from './taxonomies.controller';
import { DatabaseModule } from 'src/database/database.module';
import { permacultProviders } from 'src/database/database.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [TaxonomiesController],
  providers: [TaxonomiesService],
})
export class TaxonomiesModule {}
