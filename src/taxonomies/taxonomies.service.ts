import { Inject, Injectable } from '@nestjs/common';
import { DATABASE_CONNECTION, TAXONOMY_REPOSITORY } from 'src/shared/constants';
import { DataSource, FindManyOptions, Repository } from 'typeorm';
import { CreateTaxonomyDto } from '../shared/dto/create-taxonomy.dto';
import { UpdateTaxonomyDto } from '../shared/dto/update-taxonomy.dto';
import { Taxonomy } from '../shared/entities/permacult/taxonomy.entity';

@Injectable()
export class TaxonomiesService {
  constructor(
    @Inject(DATABASE_CONNECTION)
    private readonly connection: DataSource,
    @Inject(TAXONOMY_REPOSITORY)
    private readonly taxonomyRepository: Repository<Taxonomy>,
  ) {}

  async create(createTaxonomyDto: CreateTaxonomyDto): Promise<Taxonomy> {
    // return 'This action adds a new taxonomy';

    return this.taxonomyRepository.create(createTaxonomyDto).save();
  }

  async findAll(
    options: FindManyOptions<Taxonomy> = undefined,
  ): Promise<Taxonomy[]> {
    return await this.taxonomyRepository.find(options);

    // return `This action returns all taxonomies`;
  }

  findOne(id: number) {
    return `This action returns a #${id} taxonomy`;
  }

  update(id: number, updateTaxonomyDto: UpdateTaxonomyDto) {
    return `This action updates a #${id} taxonomy`;
  }

  remove(id: number) {
    return `This action removes a #${id} taxonomy`;
  }
}
