import { Module } from '@nestjs/common';
import {
  cultProviders,
  databaseProviders,
  nlmTaxonomyProviders,
  permacultProviders,
} from './database.providers';

@Module({
  providers: [
    ...databaseProviders,
    ...cultProviders,
    ...permacultProviders,
    ...nlmTaxonomyProviders,
  ],
  exports: [
    ...databaseProviders,
    ...cultProviders,
    ...permacultProviders,
    ...nlmTaxonomyProviders,
  ],
})
export class DatabaseModule {}
