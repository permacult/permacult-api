import { dataSourceOptionsNlmSqlite } from 'ormconfig.nlm.sqlite';
import { dataSourceOptionsPermacultSqlite } from 'ormconfig.permacult.sqlite';
import {
  DATABASE_CONNECTION,
  NLM_DATABASE_CONNECTION,
  GEN_CODE_REPOSITORY,
  TAXONOMY_REPOSITORY,
  TAX_DIVISION_REPOSITORY,
  TAX_NAME_REPOSITORY,
  TAX_CULT_NODE_REPOSITORY,
  TAXON_REPOSITORY,
  CULT_NODE_REPOSITORY,
  CULT_HELP_REPOSITORY,
} from 'src/shared/constants';
import { CultHelp } from 'src/shared/entities/cult/cult-help.entity';
import { CultNode } from 'src/shared/entities/cult/cult-node.entity';
import { GenCode } from 'src/shared/entities/nlm-taxonomy/GenCode.entity';
import { TaxDivision } from 'src/shared/entities/nlm-taxonomy/TaxDivision.entity';
import { TaxName } from 'src/shared/entities/nlm-taxonomy/TaxName.entity';
import { TaxNode } from 'src/shared/entities/nlm-taxonomy/TaxNode.entity';
import { Taxon } from 'src/shared/entities/permacult/taxon.entity';
import { Taxonomy } from 'src/shared/entities/permacult/taxonomy.entity';
import { DataSource, DataSourceOptions } from 'typeorm';

// export const dataSource = new DataSource(
//   <DataSourceOptions>(<unknown>ormConfig),
// );

export const dataSource = new DataSource(
  <DataSourceOptions>dataSourceOptionsPermacultSqlite,
);

export const dataSourceNlm = new DataSource(
  <DataSourceOptions>dataSourceOptionsNlmSqlite,
);

export const databaseProviders = [
  {
    provide: DATABASE_CONNECTION,
    useFactory: async () => await dataSource.initialize(),
  },
  {
    provide: NLM_DATABASE_CONNECTION,
    useFactory: async () => await dataSourceNlm.initialize(),
  },
];

export const cultProviders = [
  {
    provide: CULT_NODE_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(CultNode),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: CULT_HELP_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(CultHelp),
    inject: [DATABASE_CONNECTION],
  },
];

export const permacultProviders = [
  {
    provide: TAXONOMY_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Taxonomy),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: CULT_NODE_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Taxonomy),
    inject: [DATABASE_CONNECTION],
  },
];

export const nlmTaxonomyProviders = [
  {
    provide: TAX_CULT_NODE_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(TaxNode),
    inject: [NLM_DATABASE_CONNECTION],
  },
  {
    provide: TAX_NAME_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(TaxName),
    inject: [NLM_DATABASE_CONNECTION],
  },
  {
    provide: TAX_DIVISION_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(TaxDivision),
    inject: [NLM_DATABASE_CONNECTION],
  },
  {
    provide: GEN_CODE_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(GenCode),
    inject: [NLM_DATABASE_CONNECTION],
  },
  {
    provide: TAXON_REPOSITORY,
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Taxon),
    inject: [NLM_DATABASE_CONNECTION],
  },
];
