import { INestApplication } from '@nestjs/common';
import { DATABASE_CONNECTION, TAXONOMY_REPOSITORY } from 'src/shared/constants';
import {
  Taxonomy,
  TaxonomyRank,
} from 'src/shared/entities/permacult/taxonomy.entity';
import { TaxonomiesService } from 'src/taxonomies/taxonomies.service';
import { Repository } from 'typeorm';

export const seeder = async (app: INestApplication) => {
  const taxonomiesService = app.get(TaxonomiesService);

  // const db = app.get(DATABASE_CONNECTION);

  // const foundOne = await db
  //   .createQueryBuilder()
  //   .select()
  //   .take(1)
  //   .execute();

  // if (foundOne) {
  //   // db not empty, we skip seeding
  //   return;
  // }

  const eukarya = await taxonomiesService.create({
    name: 'eukarya',
    rank: TaxonomyRank.DOMAIN,
    parent: null,
  });

  console.log('Created', eukarya);

  const plantae = await taxonomiesService.create({
    name: 'Plantae',
    rank: TaxonomyRank.KINGDOM,
    parent: eukarya,
  });

  console.log('Created', plantae);

  const animalia = await taxonomiesService.create({
    name: 'Animalia',
    rank: TaxonomyRank.KINGDOM,
    parent: eukarya,
  });

  console.log('Created', animalia);

  const allTaxonomies = await taxonomiesService.findAll();

  console.log('All Taxonomies', allTaxonomies);
};
