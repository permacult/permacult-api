import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

// import * as ormConfig from '../ormconfig';
import { ConfigModule } from '@nestjs/config';
import { TaxonomiesModule } from './taxonomies/taxonomies.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { NlmTaxonomyModule } from './nlm-taxonomy/nlm-taxonomy.module';
import { DatabaseModule } from './database/database.module';
import { CultModule } from './cult/cult.module';
// import dataSourceOptions from 'ormconfig';

@Module({
  imports: [
    DatabaseModule,
    ConfigModule.forRoot({
      envFilePath: ['.env', '.env.development.local', '.env.development'],
      isGlobal: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      serveRoot: '/public',
      // exclude: ['/api*'],
    }),
    // TypeOrmModule.forRoot({
    //   ...dataSourceOptions,
    // }),

    // TypeOrmModule.forRoot(ormconfig[0]), //default
    // TypeOrmModule.forRoot(ormconfig[1]), //other db

    TaxonomiesModule,
    NlmTaxonomyModule,
    CultModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
